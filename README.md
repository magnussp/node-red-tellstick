# README #

This is a plugin node for Node-Red as to allow direct control over a USB tellstick connected to the same computer.

Right now the node only supports switches not dimmers.

## What is this repository for? ##

* To control a USB [tellstick](http://www.telldus.se/products/tellstick) via [Node-red](http://nodered.org/)
* Version 0.1.0

## Installation ##
* *cd ~*
* *git clone git@bitbucket.org:magnussp/node-red-tellstick.git*
* *cd node-red-tellstick*
* *npm install . -g*
* *pm2 restart node-red*

#### Dependencies ####

* telldus-core setup and devices configured. Follow [this how-to](http://developer.telldus.se/wiki/TellStickInstallationUbuntu#Consolebasedinstallation) for RPi installation and [this how-to](http://developer.telldus.com/wiki/TellStick_conf) for configuration.
* Node-red installed and configured.

## How to use ##

* Enter the name (can be whatever)
* The device id should be the same as configured for telldus-core
* Option can be ON or OFF

![Screen Shot 2014-12-06 at 23.34.39.png](https://bitbucket.org/repo/oe5ezX/images/3011091407-Screen%20Shot%202014-12-06%20at%2023.34.39.png)